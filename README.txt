The classes in this project (currently) procedurally deform a skinned airplane mesh to make it look like it's moving its control surfaces in response to input. It does not actually do anything physics-wise, as binding controls so closly to the visual is probably not a good idea.

To use, drop an AnimatedControlManager onto the root of your model's armature and then add AnimatedControl components (one of the two included subclasses) to each bone you want to have represent a control. Once that's done, create AnimatedControlGroups and AnimatedControlBindings for each control. 

If that sounds like too much work, just click on the AnimatedControlManager's context menu and select "Best-Guess rig airplane" and the class will try its hand at getting a rig that works nicely. You should give the system a double-check afterwards just to make sure everything's okay, and the auto-rigger requires a certain naming convention.

Each bone that's representing a control must ...
-be a child of the gameobject containing the AnimatedControlManager.
-have a name with arguments seperated by underscores "_"
-start with "surf" if you want the control to rotate around it's y-axis (roll.)
-have positive deflection in unity equal *negative* rotation around the local y-axis in blender. (Don't ask me why)
-if the second argument is "elevator", "stabilator", or "elevon", the surface is treated as a pitch control.
-if the second argument is "alieron" or "elevon", the surface is treated as a roll control
-if the second argument is "rudder" the surface is treated as a yaw control.
-is the second argument is "airbrake" the surface is treated as an airbrake. (No negative deflection, but greater positive deflection.)

-if the first argument is "noz", the control is treated as a variable-geometry engine nozzle and allowed to scale up and down to represent different engine states.
-if none of the above apply, the system throws up its hands and moves on to the next child.