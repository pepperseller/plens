﻿using UnityEngine;
namespace Assets.Scripts
{
    /// <summary>
    /// maser class for all control visuals that react to input IN SOME WAY
    /// </summary>
    abstract class AnimatedControl : MonoBehaviour
    {
        //------Member Fields------//
        [Tooltip("Minimum deflection this control is bounded by")]
        [SerializeField]
        private float minDeflection;
        [Tooltip("Maximum deflection this control is bounded by")]
        [SerializeField]
        private float maxDeflection;

        /// <summary>
        /// actual deflection this control is set do
        /// </summary>
        public float deflection
        {
            get
            {
                return this._deflection;
            }
            set
            {
                this._deflection = Mathf.Clamp(value, this.minDeflection, this.maxDeflection);
            }
        }
        /// <summary>
        /// interally tracked deflection. Made serilizeable so you can play with it in the editor to test range of motion or whatever.
        /// and 
        /// </summary>
        [Tooltip("Direct control over the deflection. For testing in editor only, don't screw with this.")]
        [SerializeField]
        private float _deflection = 0;

        //------Methods------//
        /// <summary>
        /// this is mostly here for error-checking and idiot proofing
        /// </summary>
        void awake()
        {
            if (minDeflection > maxDeflection)
                throw new System.ArgumentException("Errror on " + this.gameObject.name + " animated control. Min deflection is greater than max deflection!");
        }

        private void Start()
        {
            this.OnStart();
        }

        private void Update()
        {
            this.UpdateControl();
        }

        /// <summary>
        /// called on load to get anything we need, like control null points.
        /// </summary>
        protected abstract void OnStart();
        /// <summary>
        /// called to do whatever visual we want this control to do!
        /// </summary>
        protected abstract void UpdateControl();

        /// <summary>
        /// updates this control's deflection limits
        /// </summary>
        /// <param name="minDeflection"></param>
        /// <param name="maxDeflection"></param>
        internal void SetDeflectionRange(float minDeflection, float maxDeflection)
        {
            if (minDeflection > maxDeflection)
                throw new System.ArgumentException(this.name + " min control deflection is greater than max deflection!");

            this.maxDeflection = maxDeflection;
            this.minDeflection = minDeflection;
        }
    }
}
