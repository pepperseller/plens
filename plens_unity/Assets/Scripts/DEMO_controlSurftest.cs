﻿using UnityEngine;
namespace Assets.Scripts
{
    class DEMO_controlSurftest: MonoBehaviour
    {
        //--member fields--//
        [SerializeField]
        private AnimatedControlManager controlManager;

        //--interface methods--//
        public void SetPitch(float pitch)
        {
            this.controlManager.SetAxis("pitch", pitch);
        }
        public void SetYaw(float yaw)
        {
            this.controlManager.SetAxis("yaw", yaw);
        }
        public void SetRoll(float roll)
        {
            this.controlManager.SetAxis("roll", roll);

        }
        public void SetAirbrakes(float airbrakes)
        {
            this.controlManager.SetAxis("airbrakes", airbrakes);
        }
        public void SetBurner(float burner)
        {
            this.controlManager.SetAxis("afterburners", burner);
        }
    }
}
