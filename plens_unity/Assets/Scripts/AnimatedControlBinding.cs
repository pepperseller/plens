﻿using UnityEngine;

namespace Assets.Scripts
{
    /// <summary>
    /// binds an individual control to a binding, with a blend factor so, like V-tails will work
    /// </summary>
    [System.Serializable]
    class AnimatedControlBinding
    {
        //--member fields--//
        /// <summary>
        /// control this binding is... bound to...
        /// </summary>
        [SerializeField]
        private AnimatedControl control;

        /// <summary>
        /// how much the control in question responds to input along this axis.
        /// defaults to 1 (full authority)
        /// </summary>
        [SerializeField]
        private float axisWeight = 1;

        //--construction--//
        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="control">control to bind to</param>
        /// <param name="axisWeight">binding weight</param>
        public AnimatedControlBinding(AnimatedControl control, float axisWeight = 1)
        {
            this.control = control;
            this.axisWeight = axisWeight;
        }

        //--manipulation--//
        /// <summary>
        /// deflect the control bound to this axis by a given amount, after applying weight scaling.
        /// </summary>
        /// <param name="value"></param>
        internal void SetControlDeflection(float value)
        {
            this.control.deflection = value * axisWeight;
        }
    }
}
