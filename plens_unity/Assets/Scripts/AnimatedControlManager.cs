﻿using System.Collections.Generic;

using UnityEngine;

namespace Assets.Scripts
{
    class AnimatedControlManager: MonoBehaviour
    {
        //------Member Fields------//
        [Tooltip("Control groups, one for each axis")]
        [SerializeField]
        private AnimatedControlGroup[] controlGroups;

        /// <summary>
        /// keeps track of what axis name cooresponds to what axis group. A little messy, but better I think
        /// than needing an enum and making the system super hard to expand.
        /// </summary>
        [SerializeField]
        private Dictionary<string, AnimatedControlGroup> axisDictionary = new Dictionary<string, AnimatedControlGroup>();

        //------Methods------//
        private void Start()
        {
            this.SetDictionary();
        }

        //--interaction--//
        public void SetAxis(string axis, float value)
        {
            if (axisDictionary.ContainsKey(axis))
                axisDictionary[axis].SetDeflection(value);
            else
                Debug.Log("Animated control manager '" + this.gameObject.name + "' has no axis '" + axis + "'!");
        }

        //------helper functions------//
        private void SetDictionary()
        {
            this.axisDictionary = new Dictionary<string, AnimatedControlGroup>();
            for (int i = 0; i < controlGroups.Length; i++)
            {
                //Debug.Log("added axis '" + controlGroups[i].axis + "'");
                axisDictionary.Add(controlGroups[i].axis, controlGroups[i]);
            }
        }
        
        //--editor functions--//
        [ContextMenu("Best-guess rig airplane")]
        private void RigAirplane()
        {
            Debug.Log("Attempting to rig '" + this.gameObject.name + "'. I'll take my best shot but you should double-check!");

            //create some placeholders for bindings
            List<AnimatedControlBinding> pitchSurfs = new List<AnimatedControlBinding>();
            List<AnimatedControlBinding> yawSurfs = new List<AnimatedControlBinding>();
            List<AnimatedControlBinding> rollSurfs = new List<AnimatedControlBinding>();
            List<AnimatedControlBinding> brakeSurfs = new List<AnimatedControlBinding>();
            List<AnimatedControlBinding> burnerSurfs = new List<AnimatedControlBinding>();

            this.ClearPlaneRig();


            //kind of ugly, but this gets us grandchildren and the like too.
            Transform[] children = this.gameObject.GetComponentsInChildren<Transform>();

            for (int i = 0; i < children.Length; i++)
            {
                //go child by chlid and try to figure out what kind of control each one is!
                GameObject child = children[i].gameObject;
                string[] nameArgs = child.name.ToLower().Split('_');

                //if it's a control surface, figure out what axis it should be
                if (nameArgs[0].Equals("surf"))
                {
                    //pitch
                    if (nameArgs[1].Equals("elevator") || nameArgs[1].Equals("stabilator") || nameArgs.Equals("elevon"))
                    {
                        Debug.Log("I think " + child + " is a pitch surface!");

                        AnimatedSurface surf = child.AddComponent<AnimatedSurface>();
                        surf.SetDeflectionRange(-25f, 25f);

                        //add the newly discovered control surface to the control binding! The bit with the transform is there because
                        //mirroring bones in blender flips them, so if you want both pitch surfaces to act in concert the left needs to have its value flipped.
                        pitchSurfs.Add(new AnimatedControlBinding(surf, -Mathf.Sign(child.transform.localPosition.x)));
                    }
                    else if (nameArgs[1].Equals("rudder"))
                    {
                        Debug.Log("I think " + child + " is a yaw surface!");
                        AnimatedSurface surf = child.AddComponent<AnimatedSurface>();
                        surf.SetDeflectionRange(-25f, 25f);

                        //add the newly discovered control surface to the control binding
                        yawSurfs.Add(new AnimatedControlBinding(surf, 1));
                    }
                    else if (nameArgs[1].Equals("alieron") || nameArgs[1].Equals("elevon"))
                    {
                        Debug.Log("I think " + child + " is a roll surface!");
                        AnimatedSurface surf = child.AddComponent<AnimatedSurface>();
                        surf.SetDeflectionRange(-25f, 25f);

                        //add the newly discovered control surface to the control binding
                        rollSurfs.Add(new AnimatedControlBinding(surf, 1));
                    }
                    else if (nameArgs[1].Equals("airbrake"))
                    {
                        Debug.Log("I think " + child + " is an airbrake");                       


                        AnimatedSurface surf = child.AddComponent<AnimatedSurface>();
                        surf.SetDeflectionRange(0f, 60);

                        //add the newly discovered control surface to the control binding
                        brakeSurfs.Add(new AnimatedControlBinding(surf, 1));
                    }
                }
                else if (nameArgs[0].Equals("noz"))
                {
                    Debug.Log("I think " + child + " is an afterburner nozzle!");

                    AnimatedNozzle surf = child.AddComponent<AnimatedNozzle>();
                    surf.SetDeflectionRange(-0.5f, 0.1f);

                    //add the newly discovered control surface to the control binding
                    burnerSurfs.Add(new AnimatedControlBinding(surf, 1));
                }
                else
                {
                    Debug.Log("I don't know what " + child + "is.");
                }
            }

            //we've set all the surfaces, now apply them
            List<AnimatedControlGroup> groups = new List<AnimatedControlGroup>();
            groups.Add(new AnimatedControlGroup("pitch", pitchSurfs.ToArray()));
            groups.Add(new AnimatedControlGroup("yaw", yawSurfs.ToArray()));
            groups.Add(new AnimatedControlGroup("roll", rollSurfs.ToArray()));

            groups.Add(new AnimatedControlGroup("airbrakes", brakeSurfs.ToArray()));
            groups.Add(new AnimatedControlGroup("afterburners", burnerSurfs.ToArray()));

            this.controlGroups = groups.ToArray();
        }

        [ContextMenu("Clear plane rig")]
        void ClearPlaneRig()
        {
            //kind of ugly, but this gets us grandchildren and the like too.
            Transform[] children = this.gameObject.GetComponentsInChildren<Transform>();

            for (int i = 0; i < children.Length; i++)
            {
                //go child by chlid and try to figure out what kind of control each one is!
                GameObject child = children[i].gameObject;

                //scrub the gameobject clean!
                AnimatedControl[] control = child.GetComponents<AnimatedControl>();
                if (control != null)
                    for (int j = 0; j < control.Length; j++)
                        DestroyImmediate(control[j]);
            }

            this.controlGroups = new AnimatedControlGroup[0];
        }

    }
    
}
