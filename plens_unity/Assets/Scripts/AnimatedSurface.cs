﻿using UnityEngine;

namespace Assets.Scripts
{
    /// <summary>
    /// subclass of animated control for control surfaces. This rotates the gameobject it's attached to around
    /// the local Y-axis. (that is, it rolls the surface).
    /// </summary>
    class AnimatedSurface : AnimatedControl
    {
        //----member fields----//
        private Quaternion restPosition;

        protected override void OnStart()
        {
            this.restPosition = transform.localRotation;
        }

        /// <summary>
        /// handles the actual control rolling.
        /// </summary>
        protected override void UpdateControl()
        {
            this.transform.localRotation = (restPosition * Quaternion.AngleAxis(this.deflection, Vector3.up));
        }

       
    }
}
