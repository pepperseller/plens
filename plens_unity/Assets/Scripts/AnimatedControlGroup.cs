﻿using UnityEngine;

namespace Assets.Scripts
{
    /// <summary>
    /// manages a set of animated controls that are all ganged to the same axis in some way
    /// </summary>
    [System.Serializable]
    class AnimatedControlGroup
    {
        //--member fields--//
        /// <summary>
        /// axis this control group is bound to
        /// </summary>
        public string axis { get { return this._axis; } }

        /// <summary>
        /// internal axis field. Made a serializefield so you can set it in the editor.
        /// </summary>
        [SerializeField]
        private string _axis = "unassigned axis";

        /// <summary>
        /// controls bound to this axis
        /// </summary>
        [SerializeField]
        private AnimatedControlBinding[] controls;

        //--construction--//
        public AnimatedControlGroup(string axis, AnimatedControlBinding[] controls)
        {
            this._axis = axis;
            this.controls = controls;
        }


        /// <summary>
        /// deflects all the controls in this group
        /// </summary>
        /// <param name="value"></param>
        public void SetDeflection(float value)
        {
            for(int i = 0; i < controls.Length; i++)
            {
                controls[i].SetControlDeflection(value);
            }
        }

    }

}
