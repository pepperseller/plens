﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DEMO_cameraControls : MonoBehaviour
{

    //--tweakables--//
    [SerializeField] private float minZoom = 1;
    [SerializeField] private float maxZoom = 25;
    [SerializeField] private float initialZoom = 5;

    [SerializeField] private float xSense = 5;
    [SerializeField] private float ySense = 5;
    [SerializeField] private float zoomSense = 20;
    private float zoom;
    private float x;
    private float y;

    private void Start()
    {
        this.zoom = initialZoom;
        this.x = -90f;
    }

    private void Update()
    {
        if (Input.GetMouseButton(1))
        {
            x += Input.GetAxis("Mouse X") * xSense;
            y -= Input.GetAxis("Mouse Y") * ySense;
        }

        zoom -= Input.GetAxis("Mouse ScrollWheel") * zoomSense;

        zoom = Mathf.Clamp(zoom, minZoom, maxZoom);

        y = Mathf.Clamp(y, -89f, 89f);

        Quaternion rot = Quaternion.Euler(y, x, 0);
        Vector3 offset = new Vector3(0, 0, -zoom);

        transform.position = rot * offset;
        transform.rotation = rot;
    }
}

