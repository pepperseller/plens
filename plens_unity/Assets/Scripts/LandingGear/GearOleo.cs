﻿using UnityEngine;

namespace Assets.Scripts.LandingGear
{
    /// <summary>
    /// links two bones together so they'll always look at each other
    /// </summary>
    class GearOleo: MonoBehaviour
    {
        [SerializeField] private Transform upper;
        [SerializeField] private Transform lower;

        //--mono methods--//
        private void Update()
        {
            //upper
            HalfLookAt(upper, lower.position);
            HalfLookAt(lower, upper.position);
        }

        //--helper method--//
        private void HalfLookAt(Transform half, Vector3 target)
        {
            Vector3 localTarget = target - half.position;
            half.up = localTarget;
        }
    }
}
