﻿using UnityEngine;

namespace Assets.Scripts.LandingGear
{
    class GearStrutLateral: MonoBehaviour
    {
        //--member fields--//
        [SerializeField] private WheelCollider wheel;
        [SerializeField] private Transform strut;

        //--cached--//
        private Vector3 zeroStrutVector;
        private Quaternion strutrot;

        private Vector3 wheelPosition
        {
            get
            {
                Transform wheelTransform = wheel.transform;
                Vector3 wheelCenterWorld = wheelTransform.TransformPoint(wheel.center);

                Vector3 suspensionRay = wheelTransform.TransformVector(Vector3.up * -wheel.suspensionDistance);
                Vector3 wheelPosWorld = wheelCenterWorld + suspensionRay;


                //check if we're touching the ground!
                RaycastHit hit;
                Vector3 suspentionTestRay = wheelTransform.TransformVector(Vector3.up * -wheel.radius);

                Debug.DrawRay(wheelPosWorld, suspentionTestRay);

                if (Physics.Raycast(wheelPosWorld, suspentionTestRay, out hit, -suspentionTestRay.y))
                {
                    wheelPosWorld = hit.point -suspentionTestRay;
                    Debug.DrawRay(hit.point, Vector3.forward);
                }

                //Debug.DrawRay(wheelPosWorld, Vector3.up, Color.blue);

                return wheelPosWorld;
            }
        }
        private Vector3 wheelRestPosition
        {
            get
            {
                Transform wheelTransform = wheel.transform;
                Vector3 wheelCenterWorld = wheelTransform.TransformPoint(wheel.center);
                return wheelCenterWorld;
            }
        }

        //----methods----//
        //--mono methods--//
        private void Start()
        {
            Vector3 strutToWheel = transform.InverseTransformDirection(wheelRestPosition - transform.position);
            this.zeroStrutVector = strutToWheel;

            this.strutrot = strut.localRotation;
        }

        private void Update()
        {
            Vector3 wheelDir = transform.InverseTransformDirection(wheelPosition - transform.position);

            Quaternion offset = Quaternion.FromToRotation(zeroStrutVector, wheelDir);

            this.strut.localRotation = offset * strutrot;
        }


        //--debugging--//
        private void OnDrawGizmos()
        {
            /*
            Gizmos.DrawRay(transform.position, transform.forward);
            Gizmos.DrawLine(transform.position, wheelPosition);

            Gizmos.DrawWireSphere(wheelPosition, 0.1f);

            Gizmos.color = Color.red;
            Gizmos.DrawRay(wheelRestPosition, transform.right);
            Gizmos.DrawRay(wheelRestPosition, -transform.right);
            */
        }
    }
}
