﻿using UnityEngine;

namespace Assets.Scripts.LandingGear
{
    [RequireComponent(typeof(WheelCollider))]
    class GearStrut: MonoBehaviour
    {
        //----member fields----//
        private WheelCollider wheel;
        private Vector3 offset;

        public Transform strut;

        private void Awake()
        {
            this.wheel = GetComponent<WheelCollider>();
            this.offset = strut.localPosition;
        }

        private void Update()
        {
            Vector3 wheelCenter = wheel.transform.TransformPoint(wheel.center);
            RaycastHit hit;
            float sprungY;

            if (Physics.Raycast(transform.position, -transform.up, out hit, wheel.suspensionDistance + wheel.radius))
            {
                Vector3 point = transform.InverseTransformPoint(hit.point);
                sprungY = point.y + 2 * wheel.radius;
            }
            else
            {
                sprungY = -wheel.suspensionDistance + wheel.radius;
            }

            strut.localPosition = offset + transform.up * -sprungY;
        }
    }
}
