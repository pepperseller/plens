﻿using UnityEngine;
namespace Assets.Scripts.LandingGear
{
    class BoneLookAt: MonoBehaviour
    {
        //------Member Fields------//
        public Transform targetTransform;

        //------Construction------//

        //------Methods------//
        private void Update()
        {
            Quaternion correction = Quaternion.FromToRotation(Vector3.up, Vector3.forward);

            Vector3 target = targetTransform.position - transform.position;
            transform.up = (target);

        }

        //------testing/debugging------//
    }
}
