﻿using System;
using UnityEngine;

namespace Assets.Scripts
{
    class AnimatedNozzle : AnimatedControl
    {
        //------Member Fields------//
        private Vector3 restPoint;

        //------Methods------//
        protected override void OnStart()
        {
            this.restPoint = transform.localScale;
        }

        protected override void UpdateControl()
        {
            this.transform.localScale = (restPoint * (1 + this.deflection));
        }
    }
}
